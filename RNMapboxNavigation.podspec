
Pod::Spec.new do |s|
  s.name         = "RNMapboxNavigation"
  s.version      = "1.0.0"
  s.summary      = "RNMapboxNavigation"
  s.description  = <<-DESC
                  RNMapboxNavigation
                   DESC
  s.homepage     = ""
  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }
  s.author             = { "author" => "author@domain.cn" }
  s.platform     = :ios, "7.0"
  s.source       = { :git => "https://github.com/author/RNMapboxNavigation.git", :tag => "master" }
  s.homepage       = "https://github.com/author/RNMapboxNavigation.git"
  s.source_files  = "RNMapboxNavigation/**/*.{h,m}"
  s.requires_arc = true


  s.dependency "React"
  #s.dependency "others"

end

  