//package com.rnmapboxnavigation.Mapbox;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Handler;
//import android.os.Looper;
//import android.util.Log;
//
//import com.facebook.react.bridge.ActivityEventListener;
//import com.facebook.react.bridge.Callback;
//import com.facebook.react.bridge.ReactApplicationContext;
//import com.facebook.react.bridge.ReactContextBaseJavaModule;
//import com.facebook.react.bridge.ReactMethod;
//import com.facebook.react.bridge.ReadableArray;
//import com.mapbox.api.directions.v5.models.DirectionsResponse;
//import com.mapbox.geojson.Point;
//import com.mapbox.mapboxsdk.Mapbox;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher;
//import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions;
//import com.mapbox.services.android.navigation.v5.navigation.NavigationRoute;
//import com.rnmapboxnavigation.R;
//
//import retrofit2.Call;
//import retrofit2.Response;
//
//public class MapboxNavigationManager extends ReactContextBaseJavaModule implements ActivityEventListener {
//
//    private Context context;
//    Callback success;
//
//    public MapboxNavigationManager(ReactApplicationContext reactContext) {
//        super(reactContext);
//        this.context = reactContext;
//    }
//
//    @Override
//    public String getName() {
//        return "MapboxNavigation";
//    }
//
//
//    @ReactMethod
//    public void showNavigation(ReadableArray  destination, Callback successCallback) {
//        success = successCallback;
//        Intent intent = new Intent(getCurrentActivity(), MapboxNavigation.class);
//        getCurrentActivity().startActivityForResult(intent,405);
//    }
//
//
//    @Override
//    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
//        if(requestCode == 405){
//            success.invoke(data
//            .getStringExtra("lat")+","+data
//                    .getStringExtra("lng"));
//        }
//    }
//
//    @Override
//    public void onNewIntent(Intent intent) {
//
//    }
//}

package com.reactlibrary.Mapbox;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableArray;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.services.android.navigation.ui.v5.listeners.NavigationListener;

public class MapboxNavigationManager extends ReactContextBaseJavaModule  implements  ActivityEventListener{

    Callback successCallback;

    public MapboxNavigationManager(ReactApplicationContext reactContext) {
        super(reactContext);
        reactContext.addActivityEventListener(this);

//        LocationEngineProvider provider = new LocationEngineProvider(reactContext);
//        locationEngine = provider.obtainBestLocationEngineAvailable();

    }

    @Override
    public String getName() {
        return "RNMapboxNavigation";
    }

    @ReactMethod
    public void showNavigation(ReadableArray destination, Callback success) throws SecurityException {
        final Activity activity = getCurrentActivity();
        if (activity == null) {
            return;
        }
        successCallback = success;
        Intent i = new Intent(getCurrentActivity(),MapboxNavigation.class);
        getCurrentActivity().startActivityForResult(i,405);

//        callback = success;
//
//        Location lastKnownLocation = locationEngine.getLastLocation();
//        location = lastKnownLocation;
//        System.out.println(lastKnownLocation);
//        NavigationRoute.builder(activity)
//                .accessToken(Mapbox.getAccessToken())
//                .origin(Point.fromLngLat(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude()))
//                .destination(Point.fromLngLat(destination.getDouble(1), destination.getDouble(0)))
//                .build()
//                .getRoute(new Callback<DirectionsResponse>() {
//                    @Override
//                    public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
//                        NavigationLauncherOptions options = NavigationLauncherOptions.builder()
//                                .directionsRoute(response.body().routes().get(0))
//                                .shouldSimulateRoute(false)
//                                .build();
//                        NavigationLauncher.startNavigation(activity, options);
//                    }
//
//                    @Override
//                    public void onFailure(Call<DirectionsResponse> call, Throwable t) {
//                        Log.d(REACT_CLASS, t.getLocalizedMessage());
//                    }
//                });
    }


    @Override
    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        successCallback.invoke(data.getStringExtra("lat")+","+data.getStringExtra("lng"));
    }

    @Override
    public void onNewIntent(Intent intent) {

    }
}