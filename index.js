
import { NativeModules } from 'react-native';

const { RNMapboxNavigation } = NativeModules;

export default RNMapboxNavigation;
